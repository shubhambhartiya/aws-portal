



# gcloud compute --project=secret-well-384208 images create copy-image --source-image=election-ss-server-spot --source-image-project=forbinary-project

# gcloud compute images list --project="forbinary-project" --format="csv(name)" --quiet
 

#!/bin/bash

# Source and target project IDs
SOURCE_PROJECT="forbinary-project"
TARGET_PROJECT="secret-well-384208"
KEYWORD="fatakpay"  # Keyword to filter disk names


IMAGES_LIST=$(gcloud compute images list --project="$SOURCE_PROJECT" --format="csv(name)" --quiet)

for IMAGE_NAME in $IMAGES_LIST; do


  if [[ "$IMAGE_NAME" == *"$KEYWORD"* ]]; then
    # Copy the image from the source project to the target project
    echo $IMAGE_NAME $TARGET_PROJECT $SOURCE_PROJECT $IMAGE_NAME
    gcloud compute --project="$TARGET_PROJECT" images create "$IMAGE_NAME"  \
    --source-image="$IMAGE_NAME" \
    --source-image-project="$SOURCE_PROJECT"
  fi
done

echo "Images Copy completed."




