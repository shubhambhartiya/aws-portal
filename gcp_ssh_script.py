import subprocess
from google.cloud import compute_v1
from google.oauth2 import service_account
from tabulate import tabulate



class GCPLoginScript():

    ## service account file path
    service_account_key_path = "/home/codezenfounder/forbinary-project-059080708077.json"
    gcp_ec2_instance_set = []
    table_index = 1

    ### project configuration
    project_id = "forbinary-project"
    zone = ["asia-south1-a", "asia-south1-b", "asia-south1-c", "asia-south2-a", "asia-south2-b", "asia-south2-b", "asia-southeast1-b", "us-central1-a"]


    def __init__(self):

        ### fetch the instance list
        try:
            self.fetch_instance_list()
        except Exception as e:
            self.error_handler(e, "While fetching instance")

        ### perform the ssh connection
        try:
            self.perfrom_ssh_connection()
        except Exception as e:
            self.error_handler(e, "While making connection")



    def perfrom_ssh_connection(self):
        instance_index = int(input("Enter the Index to SSH into: "))

        ### connect with the ssh command
        ssh_command = f"ssh -i /home/codezenfounder/private aws_server@{self.gcp_ec2_instance_set[instance_index - 1][2]}"
        subprocess.run(ssh_command, shell=True, text=True)



    def fetch_instance_list(self):
        credentials = service_account.Credentials.from_service_account_file(self.service_account_key_path)

        i = 0
        a_row = []
        for a_zone in self.zone:

            compute = compute_v1.InstancesClient(credentials=credentials)
            instance_set = compute.list(request={"project": self.project_id, "zone": a_zone})

            
            for a_instance in instance_set:

                # print(a_instance)

                a_row.append(self.table_index)
                a_row.append(a_instance.name)
                a_row.append(a_instance.network_interfaces[0].access_configs[0].nat_i_p)

                self.table_index += 1

                i = i + 1
                if i == 3:
                    i = 0
                    self.gcp_ec2_instance_set.append(a_row)
                    a_row = []




        ### show in table format
        table_header = ["Index", "Server Name", "IP Address", "Index", "Server Name", "IP Address", "Index", "Server Name", "IP Address"]
        server_table = tabulate(self.gcp_ec2_instance_set, headers=table_header, tablefmt="grid")
        print(server_table)



    def error_handler(self, error_message, method):
        print("Error -", method, str(error_message))
        print("Shutting Down the script !!!!!!!")


if __name__ == "__main__":
    GCPLoginScript()