sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install nginx -y
sudo apt-get install supervisor -y

sudo apt-get install build-essential checkinstall -y
sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev -y
cd /usr/src
sudo wget https://www.python.org/ftp/python/3.5.6/Python-3.5.6.tgz
sudo tar xzf Python-3.5.6.tgz
cd Python-3.5.6
sudo ./configure --enable-optimizations
sudo make altinstall

cd

sudo apt-get install python3-pip -y
sudo pip3 install virtualenv 
sudo apt install virtualenv -y

export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"

sudo apt-get install mysql-server -y
sudo apt-get install python-dev python3-dev -y
sudo apt-get install libmysqlclient-dev -y
sudo apt-get install lsof -y

sudo apt install postgresql postgresql-contrib -y
