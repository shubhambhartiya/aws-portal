
#!/bin/bash

# Set your project ID and desired image name prefix
PROJECT_ID="forbinary-project"
KEYWORD="fatakpay"  # Keyword to filter disk names




# List all disks in your project and their zones
DISK_LIST=$(gcloud compute disks list --project="$PROJECT_ID" --format="csv(name,zone)" --quiet)



# Loop through the disks and create images
IFS=$'\n'
for DISK_INFO in $DISK_LIST; do
  DISK_NAME=$(echo "$DISK_INFO" | cut -d ',' -f1)
  PRE_ZONE=$(echo "$DISK_INFO" | cut -d ',' -f2)
  ZONE=$(echo "$PRE_ZONE" | rev | cut -d'/' -f1 | rev)
  IMAGE_NAME="$DISK_NAME"

  if [[ "$DISK_NAME" == *"$KEYWORD"* ]]; then
    echo "Creating image $IMAGE_NAME from disk $DISK_NAME in zone $ZONE"
    gcloud compute images create "$IMAGE_NAME" \
      --source-disk="$DISK_NAME" \
      --source-disk-zone="$ZONE" \
      --project="$PROJECT_ID" --force
  else
    echo "Skipping disk $DISK_NAME as it does not contain the keyword."
  fi
done

echo "Images creation completed."


