#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse,os, sys
import simplejson as json
from os.path import expanduser
import subprocess
from google.cloud import compute_v1
from google.oauth2 import service_account
from tabulate import tabulate


zones_master = '["asia-south1-a", "asia-south1-b", "asia-south1-c", "asia-south2-a", "asia-south2-b", "asia-south2-b", "asia-southeast1-b", "us-central1-a", "us-west1-b", "southamerica-west1-a", "asia-southeast2-a"]'

def get_ip_address_and_login(json_data, instance_sr_no, ssh_key_path):
    print (home)
    for instance in json_data:
        if int(json_data.index(instance))==int(instance_sr_no):
            if instance['ip_address']:
                print ("Logging into the instance "+instance['name'] + " (" + instance['ip_address']+") using key path "+ ssh_key_path)
                cmd = "ssh -i "+ssh_key_path+" aws_server@"+instance['ip_address']
                os.system(cmd)
                sys.exit(0)
            else:
                print ("Intance ip address cannot be found. Exiting...")
                exit(0)

def showing_data_to_user(json_data, ssh_key_path):
    

    print("\x1b[2J\x1b[H") #Clear terminal
    template = "| {0:3} | {1:30} | {2:15} |" # column widths: 30, 15
    print ("+" + "-"*89 + "+")

    print (template.format("No.", "Instance Name", "Instance IP")) # Print Header
    print ("+" + "="*89 + "+")
    for instance in json_data:
        if search:
            if search in instance['name'].lower():
                print (template.format(int(json_data.index(instance))+1, instance['name'], instance['ip_address']))
        else:
            try:
                print (template.format(int(json_data.index(instance))+1, instance['name'], instance['ip_address']))
            except:
                print("issue")




    print ("+" + "-"*89 + "+")
    print ("Enter 0 for exit. \n")
    instance_sr_no = input("Please enter Sr. No of an instance in which you want to login : ")
    if instance_sr_no.isdigit():
        if int(instance_sr_no)==0:
            print ("Exiting...")
            exit(0)
        else:
            instance_sr_no = int(instance_sr_no)-1
            get_ip_address_and_login(json_data, instance_sr_no, ssh_key_path)
    else:
        print ("Input has to be an interger.\nExiting...")

# def getting_json_data(ssh_key_path):
#     with open(home+"/.gcp_login") as json_file:
#         json_data = json.load(json_file)
#         showing_data_to_user(json_data['instances'], ssh_key_path)

# def set_ssh_key_path():
#     with open(home+"/.gcp_login") as json_file:
#         json_data = json.load(json_file)
#         showing_data_to_user(json_data['instances'], json_data['ssh_key_path'])

def write_instances_to_file():
    updated_data = []
    jsonFile = open(home+"/.gcp_login", "r")
    data = json.load(jsonFile)
    
    for an_account in data:

        gcp_account_name = an_account['gcp_account_name']
        gcp_project_id = an_account['gcp_project_id']
        service_account_key_path = an_account['service_account_key_path']
        ssh_key_path = an_account["ssh_key_path"]
        zones = an_account['zones']

        Instance_lists = []

        for a_zone in json.loads(an_account['zones']) :
            credentials = service_account.Credentials.from_service_account_file(an_account['service_account_key_path'])
            print(gcp_project_id, "gcp_project_id")
            compute = compute_v1.InstancesClient(credentials=credentials)
            instance_set = compute.list(request={"project": gcp_project_id, "zone": a_zone})
            for a_instance in instance_set:
                a_row = {}
                a_row['name']=a_instance.name
                a_row['ip_address']=a_instance.network_interfaces[0].access_configs[0].nat_i_p
                Instance_lists.append(a_row)

        instances_json = Instance_lists
        print ("\nWrting all the instances to gcp_login file.")
        an_account['instances'] = instances_json
        updated_data.append(an_account)
    jsonFile = open(home+"/.gcp_login", "w+")
    jsonFile.write(json.dumps(updated_data))
    jsonFile.close()


def get_corresponding_account(serial_number, ssh_key_path):
    with open(home+"/.gcp_login") as json_file:
        json_data = json.load(json_file)
        corresponding_account = json_data[serial_number]

        if ssh_key_path:
            ssh_key_path_final = ssh_key_path
        else:
            ssh_key_path_final = corresponding_account["ssh_key_path"]
        instances = corresponding_account["instances"]
        showing_data_to_user(instances, ssh_key_path_final)


def ask_for_account(home, ssh_key_path):
    corresponding_accounts_to_show = []
    with open(home+"/.gcp_login") as json_file:
        json_data = json.load(json_file)

        i = 1
        for an_account in json_data:
            corresponding_data = {}
            corresponding_data["index"] = i 
            corresponding_data["account_name"] = an_account["gcp_account_name"]
            corresponding_accounts_to_show.append(corresponding_data)
            i = i + 1

        print("\x1b[2J\x1b[H")#Clear terminal
        template = "| {0:3} | {1:30} |" # column widths: 30, 12, 10, 15
        print ("+" + "-"*38 + "+")
        print (template.format("No.", "Account Name")) # Print Header
        print ("+" + "="*38 + "+")
        for instance in corresponding_accounts_to_show:
            print (template.format(instance['index'], instance['account_name']))
        print ("+" + "-"*38 + "+")
        print ("Enter 0 for exit. \n")
        instance_sr_no = input("Please enter Sr. No of an account for its instances to show : ")
        if instance_sr_no.isdigit():
            if int(instance_sr_no)==0:
                print ("Exiting...")
                exit(0)
            else:
                instance_sr_no = int(instance_sr_no) - 1
                get_corresponding_account(instance_sr_no, ssh_key_path)
        else:
            print ("Input has to be an interger.\nExiting...")

def make_newconfig(home):
    accounts_data = []
    print("\x1b[2J\x1b[H") #Clear terminal
    number_of_accounts = input("How many accounts do you want to add? ")

    for x in range(int(number_of_accounts)):
            data = {}
            Instance_lists = []

            data['gcp_account_name'] = input("Please enter GCP Account Name "+ str(x+1) +": ")
            data['gcp_project_id'] = input("Please enter your GCP Project ID: ")
            data['service_account_key_path'] = input("Please enter your service_account_path: ")
            data['ssh_key_path'] = input("Please enter your private SSH Key Path : ")
            data["zones"] = zones_master

            # secret-well-384208
            # /home/codezenfounder/secret-well-384208-2589be9e6574.json
            # /home/codezenfounder/private

            # data['gcp_account_name'] = "Forbinary" 
            # # input("Please enter GCP Account Name "+ str(x+1) +": ")
            # data['gcp_project_id'] = 'forbinary-project'
            # # input("Please enter path of GCP Project ID : ")
            # data['service_account_key_path'] = "/home/codezenfounder/forbinary-project-059080708077.json"
            # # data['service_account_key_path'] = input("Please enter path of GCP Service Account Key : ")
            # # data['ssh_key_path'] = input("Please enter your private SSH Key Path : ")
            # data['ssh_key_path'] = "/home/codezenfounder/private"
            # data["zones"] = '["asia-south1-a", "asia-south1-b", "asia-south1-c", "asia-south2-a", "asia-south2-b", "asia-south2-b", "asia-southeast1-b", "us-central1-a"]'
            # # data['zones'] = input("Please enter your Zones - [] Array format : ")

            print(data)
            x = json.loads(data['zones'])

            credentials = service_account.Credentials.from_service_account_file(data['service_account_key_path'])
            for a_zone in json.loads(data['zones']) :
                compute = compute_v1.InstancesClient(credentials=credentials)
                instance_set = compute.list(request={"project": data['gcp_project_id'], "zone": a_zone})
                for a_instance in instance_set:
                    a_row = {}
                    a_row['name']=a_instance.name
                    a_row['ip_address']=a_instance.network_interfaces[0].access_configs[0].nat_i_p
                    Instance_lists.append(a_row)
            instances_json = Instance_lists
            data['instances'] = instances_json
            accounts_data.append(data)

    jsonFile = open(home+"/.gcp_login", "w+")
    jsonFile.write(json.dumps(accounts_data))
    os.system("chmod 600 "+home+"/.gcp_login")
    jsonFile.close()
    # set_ssh_key_path()
    ask_for_account(home, "")


def add_new_account(home):
    corresponding_accounts_to_show = []
    with open(home+"/.gcp_login") as json_file:
        accounts_data = json.load(json_file)

        print("\x1b[2J\x1b[H") #Clear terminal
        number_of_accounts = input("How many accounts do you want to add? ")

        for x in range(int(number_of_accounts)):
            data = {}
            Instance_lists = []
            data['gcp_account_name'] = input("Please enter GCP Account Name "+ str(x+1) +": ")
            data['gcp_project_id'] = input("Please enter your GCP Project ID: ")
            data['service_account_key_path'] = input("Please enter your service_account_path: ")
            data['ssh_key_path'] = input("Please enter your private SSH Key Path : ")
            data["zones"] = zones_master

            # secret-well-384208
            # /home/codezenfounder/secret-well-384208-2589be9e6574.json
            # /home/codezenfounder/private


            # data['gcp_account_name'] = "Forbinary" 
            # # input("Please enter GCP Account Name "+ str(x+1) +": ")
            # data['gcp_project_id'] = 'forbinary-project'
            # # input("Please enter path of GCP Project ID : ")
            # data['service_account_key_path'] = "/home/codezenfounder/forbinary-project-059080708077.json"
            # # data['service_account_key_path'] = input("Please enter path of GCP Service Account Key : ")
            # # data['ssh_key_path'] = input("Please enter your private SSH Key Path : ")
            # data['ssh_key_path'] = "/home/codezenfounder/private"
            
            # data['zones'] = input("Please enter your Zones - [] Array format : ")
            print(data)
            # x = data['zones']
            import pdb; pdb.set_trace()

            credentials = service_account.Credentials.from_service_account_file(data['service_account_key_path'])
            for a_zone in json.loads(data['zones']) :
                print(a_zone, "a_zone")
                compute = compute_v1.InstancesClient(credentials=credentials)
                instance_set = compute.list(request={"project": data['gcp_project_id'], "zone": a_zone})
                for a_instance in instance_set:
                    a_row = {}
                    a_row['name']=a_instance.name
                    a_row['ip_address']=a_instance.network_interfaces[0].access_configs[0].nat_i_p
                    Instance_lists.append(a_row)
            instances_json = Instance_lists
            data['instances'] = instances_json
            accounts_data.append(data)

        jsonFile = open(home+"/.gcp_login", "w+")
        jsonFile.write(json.dumps(accounts_data))
        os.system("chmod 600 "+home+"/.gcp_login")
        jsonFile.close()
        # set_ssh_key_path()
        ask_for_account(home, "")

def remove_corresponding_account(home):
    corresponding_accounts_to_show = []
    with open(home+"/.gcp_login") as json_file:
        json_data = json.load(json_file)

        i = 1
        for an_account in json_data:
            corresponding_data = {}
            corresponding_data["index"] = i 
            corresponding_data["account_name"] = an_account["gcp_account_name"]
            corresponding_accounts_to_show.append(corresponding_data)
            i = i + 1

        print("\x1b[2J\x1b[H") #Clear terminal
        template = "| {0:3} | {1:30} |" # column widths: 30, 12, 10, 15
        print ("+" + "-"*38 + "+")
        print (template.format("No.", "Account Name")) # Print Header
        print ("+" + "="*38 + "+")
        for instance in corresponding_accounts_to_show:
            print (template.format(instance['index'], instance['account_name']))
        print ("+" + "-"*38 + "+")
        print ("Enter 0 for exit. \n")
        instance_sr_no = input("Please enter Sr. No of an account that has to be removed : ")
        if instance_sr_no.isdigit():
            if int(instance_sr_no)==0:
                print ("Exiting...")
                exit(0)
            else:
                instance_sr_no = int(instance_sr_no) - 1
                json_data.pop(instance_sr_no)
        else:
            print ("Input has to be an interger.\nExiting...")

        jsonFile = open(home+"/.gcp_login", "w+")
        jsonFile.write(json.dumps(json_data))
        os.system("chmod 600 "+home+"/.gcp_login")
        jsonFile.close()
        # set_ssh_key_path()
        ask_for_account(home, "")

def main():
    parser = argparse.ArgumentParser(description='Login into instance')
    parser.add_argument('-c', '--cache', help='Pass "no" for calling fresh data.', nargs='?', default="yes" )
    parser.add_argument('-k', '--key', help='Provide the ssh-key-path.', nargs='?')
    parser.add_argument('-s', '--search', help='Reduces the list of .', nargs='?')
    parser.add_argument('-n', '--new_account', help='Add New Account', nargs='?')
    parser.add_argument('-r', '--remove_account', help='Remove Existing Account', nargs='?')
    args = parser.parse_args()
    global key
    global cache
    global home
    global search
    global new_account
    global remove_account
    ssh_key_path = args.key
    cache = args.cache
    search = args.search
    new_account = args.new_account
    remove_account = args.remove_account

    home = expanduser("~")
    file_exists = os.path.exists(home + "/.gcp_login")

    if str(file_exists)!="False":
        if new_account:
            add_new_account(home)
        if remove_account:
            remove_corresponding_account(home)

        if cache=="no":
            print ("User asking for new data.")
            write_instances_to_file()
        
        if ssh_key_path:
            ask_for_account(home, ssh_key_path)
        else:
            ask_for_account(home, "")
    else:
        make_newconfig(home)

if  __name__ =='__main__':
    main()
