# def detect_text(path):
#     """Detects text in the file."""
#     from google.cloud import vision

#     client = vision.ImageAnnotatorClient()

#     with open(path, "rb") as image_file:
#         content = image_file.read()

#     image = vision.Image(content=content)

#     response = client.text_detection(image=image)
#     texts = response.text_annotations
#     print("Texts:")

#     for text in texts:
#         print(f'\n"{text.description}"')

#         vertices = [
#             f"({vertex.x},{vertex.y})" for vertex in text.bounding_poly.vertices
#         ]

#         print("bounds: {}".format(",".join(vertices)))

#     if response.error.message:
#         raise Exception(
#             "{}\nFor more info on error messages, check: "
#             "https://cloud.google.com/apis/design/errors".format(response.error.message)
#         )



# path = "/home/codezenfounder/Downloads/terviva.jpeg"
# detect_text(path)






from google.cloud import vision
from google.oauth2 import service_account

def detect_labels(image_path, credentials_path):
    """Detects labels in the image file."""
    credentials = service_account.Credentials.from_service_account_file(credentials_path)
    client = vision.ImageAnnotatorClient(credentials=credentials)

    with open(image_path, 'rb') as image_file:
        content = image_file.read()

    image = vision.Image(content=content)
    response = client.document_text_detection(image=image)
    texts = response.text_annotations
    print("Texts:")

    for text in texts:
        print(f'\n"{text.description}"')

        vertices = [
            f"({vertex.x},{vertex.y})" for vertex in text.bounding_poly.vertices
        ]

        print("bounds: {}".format(",".join(vertices)))

    if response.error.message:
        raise Exception(
            "{}\nFor more info on error messages, check: "
            "https://cloud.google.com/apis/design/errors".format(response.error.message)
        )

        
    # response = client.label_detection(image=image)

    # labels = response.label_annotations
    # print('Labels:')
    # for label in labels:
    #     print(label.description)

if __name__ == '__main__':
    image_path = '/home/codezenfounder/Downloads/terviva.jpeg'
    credentials_path = '/home/codezenfounder/forbinary-project-vision-720d1476d14e.json'
    detect_labels(image_path, credentials_path)