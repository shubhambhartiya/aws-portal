#!/usr/bin/env python

import sys, os, time, json
import boto3
import paramiko

########### SETUP Credentials ###################

aws_key = os.environ["aws_key"]
aws_secret = os.environ["aws_secret"]
region = os.environ["region"]


load_balancer_arn = os.environ["agg_load_balancer"]
target_group_arn = os.environ["agg_target_group_arn"]
key_path = os.environ["agg_key_path"]
commands_to_run = ['cd /home/ubuntu/aggregation-service && git pull', 'cd /home/ubuntu/aggregation-service && npm install', 'cd /home/ubuntu/aggregation-service && pm2 restart ecosystem.json --update-env']

##########################################################

k = paramiko.RSAKey.from_private_key_file(key_path)
c = paramiko.SSHClient()
c.set_missing_host_key_policy(paramiko.AutoAddPolicy())

###### Getting ELB client #######
elb_client = boto3.client('elbv2', aws_access_key_id = aws_key, aws_secret_access_key = aws_secret, region_name = region)

print("Getting corresponding load balancer "+ load_balancer_arn)
corresponding_target_groups = elb_client.describe_target_groups(LoadBalancerArn = load_balancer_arn)

for a_target_group in corresponding_target_groups["TargetGroups"]:
    print("TargetGroupArn", a_target_group["TargetGroupArn"])
    if a_target_group["TargetGroupArn"] == target_group_arn:
        target_group_arn = a_target_group["TargetGroupArn"]

print("Getting target group instances "+target_group_arn)
targets = elb_client.describe_target_health(TargetGroupArn = target_group_arn)

for target in targets["TargetHealthDescriptions"]:
    print(target["Target"]["Id"])


    ec2 = boto3.client('ec2', aws_access_key_id = aws_key, aws_secret_access_key = aws_secret, region_name = region)
    corresponding_instance = ec2.describe_instances(
        InstanceIds = [target["Target"]["Id"]]
    )
    target_private_ip_address = corresponding_instance['Reservations'][0]['Instances'][0]['PrivateIpAddress']
    print("target_private_ip_address", target_private_ip_address)
        
    print("Deregistering " + target["Target"]["Id"])
    response = elb_client.deregister_targets(
        TargetGroupArn=target_group_arn,
        Targets=[
            {
                'Id': target["Target"]["Id"],
            },
        ]
    )
    print("Timer of 30 seconds for draining all interactions for instance " + target_private_ip_address)
    time.sleep(30) 

    c.connect( hostname = target_private_ip_address, username = "ubuntu", pkey = k )

    for command in commands_to_run:
        print "Executing {}".format( command )
        stdin , stdout, stderr = c.exec_command(command)
        print (stdout.read())
        print ("Errors" + stderr.read())
        # if stderr: 
        #     break;
    c.close()

    print("Registering " + target["Target"]["Id"])
    response = elb_client.register_targets(
        TargetGroupArn=target_group_arn,
        Targets=[
            {
                'Id': target["Target"]["Id"],
            },
        ]
    )










